## How to Contribute

If you wish to contribute, please first make an issue regarding what you would like to be implemented. Someone will then be able to pick it up or you may start it yourself. Issues do not need to be signed-off as this is a community project. The implementation of the issues is to ensure nothing is being worked on by more than one person at any given time.

## Purpose

The goal of this project is to collect all WoW Classic guide data in one central hub and then create formatters to get the data working with different websites and plugins. The data and code here can be used by anyone, both commercially or not.

## Contact

Any questions or concerns may be brought up with [pierrce](https://gitlab.com/pierrce), [jasonboukheir](https://gitlab.com/jasonboukheir), or on our issues board.